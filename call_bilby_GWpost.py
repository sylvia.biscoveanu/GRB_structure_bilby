#!/usr/bin/env python                                                          
# follow bilby example here:
# https://git.ligo.org/Monash/bilby/blob/master/examples/other_examples/gaussian_example.py
from __future__ import division
import bilby
from cal_fluence_vector import *
import sys
import re
import pdb
import numpy as np
import scipy
from scipy.misc import logsumexp
import json

# define parameters and likelihood
class GaussianLikelihood(bilby.Likelihood):
    def __init__(self, data):
        """
        Truncated Gaussian
        Parameters
        ----------
        data: array_like
            The data to analyse
        """
        self.fluence_data = data['fluence']
        self.iota = data['iota']
        self.dist = data['dist']
        self.z = data['z']
        self.log_evidence = data['log_evidence']
        self.sigma=0.3e-7
        self.noise=np.random.normal(loc=self.fluence_data,scale=self.sigma)
        self.parameters = {'log10_E0': None, 'gamma': None, 'thetaj': None, 'k': None}

    def log_likelihood(self):
        # define parameters
        E0 = np.power(10, self.parameters['log10_E0'])
        thetaj = self.parameters['thetaj']
        gamma = self.parameters['gamma']
        k = self.parameters['k']
         
        # calculate fluence
        try:
            fluence = cal_fluence(E0, gamma, thetaj, k, self.iota, self.dist, self.z)
        except:
            e = sys.exc_info()[0]
            print("Error: {}".format(e))
            return np.NINF
        
        logL = logsumexp(np.divide(np.power(np.subtract(self.noise,fluence),2),-2*self.sigma**2))
        logL += self.log_evidence - np.log(len(self.iota))
        if np.isnan(logL) or np.isinf(logL):
          print("ill logL, params were:\n")
          print(E0, gamma, thetaj, k,'---',self.fluence_data,self.noise,'---',fluence, '>>>' ,np.divide(np.power(np.subtract(self.noise,fluence),2),-2*self.sigma**2), np.log(np.sin(self.iota*np.pi/180)*np.power(self.dist, 2)))
        return logL


def main(outdir, data_file, prior):
    # load data
    event_data = json.load(open(data_file,'r'))
    
    # label and output directory
    label = 'sgrb_'+re.split('sim.json',re.split('/', data_file)[-1])[0]+prior

    likelihood = GaussianLikelihood(event_data)
    if prior == 'topHatUniformElse':
        priors = bilby.core.prior.PriorSet(filename='./topHatUniformElse.prior')
        mywalks = 15
    elif prior == 'uniformAll':
        log10_E0 = bilby.core.prior.Uniform(name='log10_E0', minimum=47, maximum=54, latex_label=r'$\logE_{0}$')
        gamma = bilby.core.prior.Uniform(name='gamma', minimum=2, maximum=299, latex_label=r'$\Gamma$')
        thetaj = bilby.core.prior.Uniform(name='thetaj', minimum=2, maximum=50, latex_label=r'$\theta_{j}$')

        def correlation_func_k(extrema_dict, gamma, thetaj):
            kMax = -np.log(1./gamma)/np.log(thetaj)
            return extrema_dict['minimum'], kMax

        k = bilby.core.prior.CorrelatedUniform(minimum=0, maximum=2, name='k', correlation_func=correlation_func_k)
        priors = bilby.core.prior.CorrelatedPriorDict(dictionary=dict(log10_E0=log10_E0, gamma=gamma, thetaj=thetaj, k=k))
        mywalks = 20
    elif prior == 'true_hyper':
        priors = bilby.core.prior.PriorSet(filename='./true_hyper.prior')
        mywalks = 15

    params = dict()
    for key in ['log10_E0', 'thetaj', 'gamma', 'k']:
        params[key] = event_data[key]

    # run sampler (nest)
    mylive = 5000
    result = bilby.run_sampler(
        likelihood=likelihood, priors=priors, injection_parameters = params, sampler='dynesty', nlive=mylive,
        evidence_tolerance=0.05, outdir=outdir, label=label, clean=False, resume=True, walks=mywalks)
    result.plot_corner(bins=20)

if __name__ == "__main__":
    if (len(sys.argv) != 4):
        print("Usage is: \n > python call_bilby_GWpost.py OUTDIR DATA_FILE PRIOR_TYPE")
        print("OUTDIR is the directory where to store the bilby output")
        print("DATA_FILE is the *sim.json file with the injections")
        print("PRIOR_TYPE is \"topHatUniformElse\", \"true_hyper\", or \"uniformAll\"")
    else:
        main(sys.argv[1], sys.argv[2], sys.argv[3])
