#! /bin/bash
# record the hostname of the node we are running on
hostname > /home/sylvia.biscoveanu/condor/logs/generateTable_$1_$2_$3.info

# limit memory to 4g (low-memory version passes the test on the head node)
echo "ulimit -v 4096000"
ulimit -v 4096000

# run python
python /home/sylvia.biscoveanu/GRBstructure/generateTable_new.py $1 $2 $3 /home/sylvia.biscoveanu/GRBstructure/tables/
