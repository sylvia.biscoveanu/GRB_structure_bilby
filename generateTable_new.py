import numpy as np
import math
import pdb
import sys
import warnings

#Default values
jetAngles = np.radians(range(2,51)) #opening angle range
ks = np.arange(0,8.1,0.1) #spectral index range
theta_c = math.radians(1) #critical angle for structured model
gamma_range = range(2,302,3)

def model(theta,phi,gamma_max,thetav,k):
	gamma = gamma_max*(theta/theta_c)**(-k)
	beta = np.sqrt(1-gamma**(-2))
        return theta**(-k)*gamma**(-4)*(1-beta*(math.cos(theta)*math.cos(thetav)+math.sin(theta)*math.sin(thetav)*math.cos(phi)))**(-3)*math.sin(theta)

def ang(gamma_max,iota,thetaj,k):
	kmax = -np.log(1./gamma_max)/np.log(thetaj/theta_c)
	if k>kmax:
		print "Gamma_min < 1 for this value of k!"
		ang = np.zeros(len(iota))
		ang[:] = np.nan
	
	else:
		theta_arr = np.linspace(0,thetaj,1000)
		phi_arr = np.linspace(0,2*np.pi,1000)
		dtheta = theta_arr[1]-theta_arr[0]
		dphi = phi_arr[1]-phi_arr[0]
		norm = 0
		for val in theta_arr:
			if val<theta_c:
				norm += math.sin(val)
			else:
				norm += val**(-k)*math.sin(val)
		norm *= 2*np.pi*dtheta

                intg = 0
                for val1 in phi_arr:
                        for val2 in theta_arr:
                                if val2<theta_c:
                                        intg +=model(val2,val1,gamma_max,iota,0)
                                else:
                                        intg += model(val2,val1,gamma_max,iota,k)
                intg *= dtheta*dphi/norm
	        ang = intg
        return ang

def main(gamma_ind, thetaj_ind, k_ind, outdir):
	vals = ang(gamma_range[int(gamma_ind)],np.radians(range(91)),jetAngles[int(thetaj_ind)],ks[int(k_ind)])	
        np.savetxt(outdir+'/table2_g'+gamma_ind+'_j'+thetaj_ind+'_k'+k_ind+'.dat', vals)

if __name__ == "__main__":
    if len(sys.argv)!=5:
        print("Usage is: \n > python generateTable_new.py gamma_ind thetaj_in k_ind outdir")
        print("Outdir is where to write the output")
    else:
        main(sys.argv[1],sys.argv[2],sys.argv[3], sys.argv[4])
