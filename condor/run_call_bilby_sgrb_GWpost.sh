#! /bin/bash
# record the hostname of the node we are running on
hostname > /home/sylvia.biscoveanu/condor/logs/bilby/call_bilby_sgrb_GWpost_$@.info

# setup Multinest and bilby
export LD_LIBRARY_PATH=/home/sylvia.biscoveanu/Stochastic/MultiNest/lib/:$LD_LIBRARY_PATH
export PATH=$PATH:$HOME/.local/bin/

# limit memory to 4g (low-memory version passes the test on the head node)
echo "ulimit -v 4096000"
# ulimit -v 4096000

# arguments and temporary output directory
OUTDIR=/home/sylvia.biscoveanu/GRB_git/salvo_src/outdir_take2/
mkdir -p $OUTDIR

DATA_FILE=/home/sylvia.biscoveanu/GRB_git/salvo_src/injections_take2/realistic_$@_sim.json
PRIOR_TYPE=topHatUniformElse

# run bilby
python /home/sylvia.biscoveanu/GRB_git/salvo_src/call_bilby_GWpost.py $OUTDIR $DATA_FILE $PRIOR_TYPE
