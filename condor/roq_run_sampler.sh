#! /bin/bash
# record the hostname of the node we are running on
hostname > /home/sylvia.biscoveanu/condor/logs/bilby/roq_run_sampler_$1.info

# setup Multinest and bilby
export LD_LIBRARY_PATH=/home/sylvia.biscoveanu/Stochastic/MultiNest/lib/:$LD_LIBRARY_PATH
export PATH=$PATH:$HOME/.local/bin/
export LAL_DATA_PATH=/home/sylvia.biscoveanu/lalsuite-extra/data/lalsimulation/:${LAL_DATA_PATH}

# limit memory to 4g (low-memory version passes the test on the head node)
echo "ulimit -v 4096000"
# ulimit -v 4096000

# run bilby
python /home/sylvia.biscoveanu/bilby_fork/examples/injection_examples/roq_run_sampler.py $1
