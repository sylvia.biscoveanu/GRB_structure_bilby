import numpy as np
import sys

def main(outdir):
    table = []
    for g in range(len(range(2,302,3))):
            for j in range(49):
                    for k in range(81):
                            vals = np.loadtxt(outdir+'/table2_g'+str(g)+'_j'+str(j)+'_k'+str(k)+'.dat')
                            table.append(vals)
    np.savetxt(outdir+'/param_table2_gamma.txt',table)

if __name__ == '__main__':
    if len(sys.argv)!=2:
        print("Usage is: \n > python concatenateTable.py OUTDIR")
        print("OUTDIR is where the individual table row files are stored and where the concatenated table will be written")
    else:
        main(sys.argv[1])
