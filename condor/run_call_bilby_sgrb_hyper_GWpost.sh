#! /bin/bash
# record the hostname of the node we are running on
hostname > /home/sylvia.biscoveanu/condor/logs/bilby/call_bilby_sgrb_hyper_GWpost_$@.info

# setup Multinest and bilby
export LD_LIBRARY_PATH=/home/sylvia.biscoveanu/Stochastic/MultiNest/lib/:$LD_LIBRARY_PATH
export PATH=$PATH:$HOME/.local/bin/

# limit memory to 4g (low-memory version passes the test on the head node)
echo "ulimit -v 4096000"
# ulimit -v 4096000

# The zeroth job always exits gracefully
if [ "$@" == 0 ]
  then exit
fi

FILE_NUMBER=$@
INJECTION_FILE=/home/sylvia.biscoveanu/GRB_git/salvo_src/injections_take2/realistic_$((FILE_NUMBER-1))_sim.json
OUTDIR=/home/sylvia.biscoveanu/GRB_git/salvo_src/outdir_take2/

# run bilby
python /home/sylvia.biscoveanu/GRB_git/salvo_src/call_bilby_hyper.py $@ $OUTDIR $INJECTION_FILE
