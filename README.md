# How to Run the Code:

Different prior files:  
binary_neutron_stars - File for running the GW PE with ROQ bounds and astrophysically motivated mass ratio  
topHatUniformElse.prior - File for running individual GRB PE, all parameters uniform except k=0  
true_hyper.prior - File for running individual GRB PE, all parameters truncated Gaussians like the hyper-prior  
hyper_pe.prior - File for running hyper-PE in conjuction with the topHatUniformElse prior  
hyper_pe_uniform.prior - File for running hyper-PE in conjunction with the uniformAll prior (see below)  

As a preliminary step, you should make sure you have bilby installed. If you want to run with the power law jet injections, you will have to use a correlated prior for k, so I recommend checking out [my branch](https://git.ligo.org/sylvia.biscoveanu/bilby/tree/correlated-priors-mine). 
Correlated priors are now implemented in master bilby, but I haven't updated the implementation in my scripts below, so it's safest to use my branch.

Example condor files can be found in the `condor` directory.
These are meant to be demonstrative and will require editing for each user. If you use condor, you will also have to edit the file paths to the prior files from relative to absolute paths. 
They executable `.sh` files in this directory contain example uses for each of the scripts described below.

## Generating the fluence lookup table
You can skip this step if you prefer to use the included lookup table, `params_table2_gamma.txt`. I recommend using the provided table, since this step will produce an identical table and is extremely time and memory consuming. 
If you want to generate your own lookup table, you will have to run `generateTable_new.py` 396900 times. This computes one row in the 4D table at a time, corresponding to the fluence for all inclinations between 0 and 90 degrees for the particular grid point specified by the indeces on the lorentz factor, opening angle, and power-law index.
```
Usage is: 
 > python generateTable_new.py gamma_ind thetaj_ind k_ind outdir
Outdir is where to write the output
```
`gamma_ind` should range from 0-99, `thetaj_ind` from 0-49, and `k_ind` from 0-81. The individual table rows will be written to the specified directory. There is an example dag file for running all rows of this process on condor in the `condor` directory. After this step, you need to assemble the table by running `concatenateTable.py`.
```
Usage is: 
 > python concatenateTable.py OUTDIR
OUTDIR is where the individual table row files are stored and where the concatenated table will be written
```
This will produce a table called `params_table2_gamma.txt` in the specified directory. You will then need to edit `cal_fluence.py` and `cal_fluence_vector.py` to point to your version of the table instead of the one included in the git repo. 
Remember that this path must also be changed to an absolute path if you want to run on condor or another cluster.

## Generating GW posteriors
I used the ROQ likelihood in bilby to generate posteriors for 100 BNS events. If you want to use my GW posteriors, they can be found on CIT here: `/home/sylvia.biscoveanu/bilby_fork/examples/injection_examples/outdir/grb_bns`.
If you want to generate your own GW posteriors, the process is split into two steps since computing the weights is very memory-intensive. 
This 2-step process has to be repeated for each of the GW BNS events you want to simulate, so I recommend doing it on condor or another cluster. 

1. Run `roq_compute_weights.py` - This will generate the injection parameters and the ROQ weights for each injection. 
```
Usage is: 
 > python roq_compute_weights.py NUMBER OUTDIR
NUMBER is the run number
OUTDIR is where the output will be written
```
2. Run `roq_run_sampler.py` - This loads the injection parameters, data, and ROQ weights generated in the first step and runs the sampler.
```
Usage is: 
 > python roq_run_sampler.py NUMBER OUTDIR
NUMBER is the run number
OUTDIR is where the output will be written
```

## Generating the GRB injections
The next step is to generate the GRB parameter injections. This is done using `generate_events_GWpost.py`, which will generate 100 injection files called `$PRIOR_$NUMBER_sim.json` that will be saved to the specified `$OUTDIR`. 
This script reads in the results files for the GW PE that you generated in the first step, which you specify as an input using `$SOURCEDIR`.
The `$PRIOR` is the prior from which to draw the injections and can be either `delta_function`, `sample_prior`, `realistic`, or `power_law`. 
I used `realistic` to generate my top-hat injections and `power_law` for the power-law injections. The others were used for testing. The output files contain the true fluence, the randomly generated Lorentz factor, opening angle, total energy, and power-law index, and the GW posteriors for distance, redshift, and inclination.
```
Usage is:
 > python generate_events.py OUTDIR PRIOR_TYPE SOURCEDIR
OUTDIR is the directory where the GRB injection files will be written
SOURCEDIR is the directory where the results files for the bilby BNS runs are stored
Available prior types are delta_function, sample_prior, realistic, and power_law
```

## Running the first step of GRB PE
Now run `call_bilby_GWpost.py` to actually run the sampler (dynesty in this case). This will read in the injection file you specify, run the sampler, and save the output to the specified directory.
The output is a bilby result file with posteriors on the opening angle, Lorentz factor, total energy, and power-law index for the specified injection. You can choose between different priors types.
`topHatUniformElse` fixes the power-law index to 0, so that it's not a sampled parameter. `true_hyper` corresponds to the truncated Gaussian distributions used in the `realstic` prior for generating the injections.
`uniformAll` uses the correlated prior on the power-law index and all the other parameters are uniform. I used `uniformAll` to get samples for all 4 parameters for both the top-hat and power-law jet injections.
```
Usage is:
 > python call_bilby_GWpost.py OUTDIR DATA_FILE PRIOR_TYPE
OUTDIR is the directory where to store the bilby output
DATA_FILE is the *sim.json file with the injections
PRIOR_TYPE is "topHatUniformElse", "true_hyper", or "uniformAll"
```

## Running hyper-PE
Once the individual GRB PE runs have finished, you can now run hyper-pe using `call_bilby_hyper.py`. This uses the native bilby hyper-PE package. 
**DISCLAIMER:** It is currently set to use the dynesty sampler, but I found that the runs never finshed with dynesty.
I had to switch to using cpnest on a cluster with slurm instead of condor, since cpnest has known checkpointing issues with condor and the jobs restart instead of resuming. I was also able to use multinest for my top-hat injections, but the results from multinest weren't well-converged for the top-hat injections, hence my switch to cpnest.
This step reads in the specified number of results files from the first step of GRB PE and produces posterior samples for the eight hyper-parameters in the model, which correspond to the means and widths of the truncated Gaussians describing each of the four individual GRB parameters.
These will also be saved to the directory with the results from the previous step of PE.
```
Usage is: 
 > python call_bilby_hyper.py EVENT_NUMBER OUTDIR DATA_FILE PRIOR
EVENT_NUMBER is the number of events to include in the analisys
OUTDIR is the directory where the output of the first step of PE is stored
DATA_FILE is the *sim.json file with the injection of the last event you want to include
PRIOR is either topHatUniformElse or uniformAll
```