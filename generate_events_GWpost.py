from cal_fluence import *
from scipy.stats import truncnorm
import sys
import bilby
import json

Mpc_to_cm = 3.086e24
h0 = 0.68
Dh = 9.26e27/h0 #hubble distance in cm
log10_E0_max = 54.
log10_E0_min = 47.

def generate_events(outdir, prior, sourcedir):

    # load the GW posteriors from LALInference BNS runs
    np.random.seed(2000)

    for number in range(100):
        print(number)
        result = bilby.core.result.read_in_result(source_dir+'roq_'+str(number)+'_result.json')

        # load the injected values of inclination and distance
        iota_inj = result.injection_parameters['theta_jn']*180/np.pi
        if (iota_inj > 90): iota_inj = 180-iota_inj
        dist_inj = result.injection_parameters['luminosity_distance']*Mpc_to_cm 
        z_inj = dist_inj/Dh
        iota_post = (result.posterior['theta_jn']*180/np.pi).tolist()
        for i in range(len(iota_post)): 
            if (iota_post[i]> 90): iota_post[i] = 180 - iota_post[i]
        dist_post = (result.posterior['luminosity_distance']*Mpc_to_cm).tolist()
        z_post = (result.posterior['luminosity_distance']*Mpc_to_cm/Dh).tolist()

        #load the evidences
        evidence = result.log_evidence

        if prior == "delta_function":

            # E0 --------------------------------------------------------------------------
            E0 = 1e50

            # theta_j ---------------------------------------------------------------------
            thetaj = 45.

            # Gamma -----------------------------------------------------------------------
            gamma = 60.

            # k ---------------------------------------------------------------------------
            k = 0

        elif prior == "sample_prior":
           
            priors = bilby.prior.PriorDict(filename='topHatUniformElse.prior')
            # E0 --------------------------------------------------------------------------
            log10_E0 = priors['log10_E0'].sample() 
            E0 = np.power(10, log10_E0)

            # theta_j ---------------------------------------------------------------------
            thetaj = priors['thetaj'].sample()

            # Gamma -----------------------------------------------------------------------
            gamma = priors['gamma'].sample()

            # k ---------------------------------------------------------------------------
            k = priors['k'].sample()

        elif prior == "realistic":

            # log10_E0, thetaj, and gamma are all truncated gaussians within their priors
            # k = 0 top-hat jet
            
            # E0 --------------------------------------------------------------------------
            log10_E0_mu = 50
            log10_E0_std = 1
            log10_E0_lower = (log10_E0_min - log10_E0_mu) / log10_E0_std
            log10_E0_upper = (log10_E0_max - log10_E0_mu) / log10_E0_std
            log10_E0 = truncnorm.rvs(log10_E0_lower, log10_E0_upper, loc=log10_E0_mu, scale=log10_E0_std)
            E0 = np.power(10, log10_E0)

            # theta_j ---------------------------------------------------------------------
            thetaj_mu = 25
            thetaj_std = 5
            thetaj_lower = (thetaj_min - thetaj_mu) / thetaj_std
            thetaj_upper = (thetaj_max - thetaj_mu) / thetaj_std
            thetaj = truncnorm.rvs(thetaj_lower, thetaj_upper, loc=thetaj_mu, scale=thetaj_std)
            
            # Gamma -----------------------------------------------------------------------
            gamma_mu = 100
            gamma_std = 50
            gamma_lower = (gamma_min - gamma_mu) / gamma_std
            gamma_upper = (gamma_max - gamma_mu) / gamma_std
            gamma = truncnorm.rvs(gamma_lower, gamma_upper, loc=gamma_mu, scale=gamma_std)

            # k ---------------------------------------------------------------------------
            k = 0

        elif prior == "power_law":
            np.random.seed(2000)
            # log10_E0, thetaj, and gamma are all truncated gaussians within their priors
            # k ~=2 power law jet
            
            # E0 --------------------------------------------------------------------------
            log10_E0_mu = 50
            log10_E0_std = 1
            log10_E0_lower = (log10_E0_min - log10_E0_mu) / log10_E0_std
            log10_E0_upper = (log10_E0_max - log10_E0_mu) / log10_E0_std
            log10_E0 = truncnorm.rvs(log10_E0_lower, log10_E0_upper, loc=log10_E0_mu, scale=log10_E0_std, size=100)
            E0 = np.power(10, log10_E0[number])

            # theta_j ---------------------------------------------------------------------
            thetaj_mu = 7.
            thetaj_std = 4.
            thetaj_lower = (thetaj_min - thetaj_mu) / thetaj_std
            thetaj_upper = (thetaj_max - thetaj_mu) / thetaj_std
            thetaj_arr = truncnorm.rvs(thetaj_lower, thetaj_upper, loc=thetaj_mu, scale=thetaj_std, size=100)
            thetaj = thetaj_arr[number]

            # Gamma -----------------------------------------------------------------------
            gamma_mu = 270.
            gamma_std = 20.
            gamma_lower = (gamma_min - gamma_mu) / gamma_std
            gamma_upper = (gamma_max - gamma_mu) / gamma_std
            gamma_arr = truncnorm.rvs(gamma_lower, gamma_upper, loc=gamma_mu, scale=gamma_std, size=100)
            gamma = gamma_arr[number]

            # k ---------------------------------------------------------------------------
            kmax = min(np.log(gamma_arr)/np.log(thetaj_arr))
            k = 1.9
            if k > kmax:
                print("k is unphysical")
      
        else:
            print("Unrecognized prior type, aborting!")
            return

        # calculate fluence for each event
        fluence = cal_fluence(E0, gamma, thetaj, k, iota_inj, dist_inj, z_inj)

        # output results to json file
        params_dict = {'fluence': fluence, 'iota': iota_post, 'dist': dist_post, 'z': z_post,\
                'log_evidence': evidence, 'log10_E0': np.log10(E0), 'thetaj': thetaj, 'gamma': gamma, 'k': k}
        json.dump(params_dict, open(outdir+prior+'_'+str(number)+'_sim.json','w'))

if __name__ == "__main__":
    if (len(sys.argv) != 3):
        print("Usage is: \n > python generate_events.py OUTDIR PRIOR_TYPE SOURCEDIR")
        print("OUTDIR is the directory where the GRB injection files will be written")
        print("SOURCEDIR is the directory where the results files for the bilby BNS runs are stored")
        print("Available prior types are delta_function, sample_prior, realistic, and power_law")

    else:
        generate_events(sys.argv[1], sys.argv[2])
