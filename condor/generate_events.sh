#! /bin/bash
# record the hostname of the node we are running on
hostname > /home/sylvia.biscoveanu/condor/logs/tupak/generate_events.info

# setup Multinest and tupak
export LD_LIBRARY_PATH=/home/sylvia.biscoveanu/Stochastic/MultiNest/lib/:$LD_LIBRARY_PATH
export PATH=$PATH:$HOME/.local/bin/

# run tupak
python /home/sylvia.biscoveanu/GRB_git/salvo_src/generate_events_GWpost.py /home/sylvia.biscoveanu/GRB_git/salvo_src/injections_take2/ realistic /home/sylvia.biscoveanu/bilby_fork/examples/injection_examples/outdir/grb_bns/
