#!/usr/bin/env python
"""
Example of how to use the Reduced Order Quadrature method (see Smith et al.,
(2016) Phys. Rev. D 94, 044031) for a Binary Black hole simulated signal in
Gaussian noise.

This requires files specifying the appropriate basis weights.
These aren't shipped with Bilby, but are available on LDG clusters and
from the public repository https://git.ligo.org/lscsoft/ROQ_data.
"""
from __future__ import division, print_function
import pdb
import numpy as np
import json
import bilby
import sys

def main(number, outdir):
    label = 'roq_'+str(number)

    # Rescale the 128s basis
    flow = 32.
    rescale_factor = 32./20
    mc_min = 1.420599/rescale_factor
    mc_max = 2.602169/rescale_factor

    # Load in the pieces for the linear part of the ROQ. Note you will need to
    # adjust the filenames here to the correct paths on your machine
    roq_matrix_directory = '/home/cbc/ROQ_data/IMRPhenomPv2/128s/'
    basis_matrix_linear = np.load(roq_matrix_directory+"B_linear.npy").T
    freq_nodes_linear = np.load(roq_matrix_directory+"fnodes_linear.npy")*rescale_factor

    # Load in the pieces for the quadratic part of the ROQ
    basis_matrix_quadratic = np.load(roq_matrix_directory+"B_quadratic.npy").T
    freq_nodes_quadratic = np.load(roq_matrix_directory+"fnodes_quadratic.npy")*rescale_factor
    params = np.genfromtxt(roq_matrix_directory+"params.dat", names=True)
    params['flow'] *= rescale_factor
    params['fhigh'] *= rescale_factor
    params['seglen'] /= rescale_factor
    params['chirpmassmin'] /= rescale_factor
    params['chirpmassmax'] /= rescale_factor
    params['compmin'] /= rescale_factor
    np.random.seed(3*number)

    duration = 128. / rescale_factor 
    sampling_frequency = 8192. * rescale_factor

    # Set up the prior
    tc = 1126259642.413+duration*number
    priors = bilby.gw.prior.PriorDict(filename='./prior_files/binary_neutron_stars.prior')
    # Set all spins to 0
    priors.pop('chi_1')
    priors.pop('chi_2')
    for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'phi_12', 'phi_jl']:
        priors[key] = 0
    for key in ['mass_1', 'mass_2']:
        priors[key] = bilby.core.prior.Constraint(params['compmin'], 3.0)
    priors['geocent_time'] = bilby.core.prior.Uniform(tc - 0.1, tc + 0.1, latex_label='$t_c$', unit='s')
    injection_parameters = priors.sample()
    distance_marginalization=True
    json.dump(injection_parameters, open(outdir+label+'_inj.json', 'w'))

    waveform_arguments = dict(waveform_approximant='IMRPhenomPv2',
                              reference_frequency=20., minimum_frequency=32.)

    waveform_generator = bilby.gw.WaveformGenerator(
        duration=duration, sampling_frequency=sampling_frequency,
        frequency_domain_source_model=bilby.gw.source.lal_binary_black_hole,
        waveform_arguments=waveform_arguments,
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters)

    ifos = bilby.gw.detector.InterferometerList(['H1', 'L1'])
    for ifo in ifos:
        ifo.minimum_frequency = flow
        ifo.maximum_frequency = sampling_frequency/2.
    ifos.set_strain_data_from_power_spectral_densities(
        sampling_frequency=sampling_frequency, duration=duration,
        start_time=injection_parameters['geocent_time'] - 2)
    ifos.inject_signal(waveform_generator=waveform_generator,
                       parameters=injection_parameters)
    for ifo in ifos: ifo.save_data(outdir, label)

    # make ROQ waveform generator
    search_waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
        duration=duration, sampling_frequency=sampling_frequency,
        frequency_domain_source_model=bilby.gw.source.binary_black_hole_roq,
        waveform_arguments=dict(frequency_nodes_linear=freq_nodes_linear,
                                frequency_nodes_quadratic=freq_nodes_quadratic,
                                reference_frequency=20., minimum_frequency=32.,
                                approximant='IMRPhenomPv2'),
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters)

    likelihood = bilby.gw.likelihood.ROQGravitationalWaveTransient(
        interferometers=ifos, waveform_generator=search_waveform_generator,
        linear_matrix=basis_matrix_linear, quadratic_matrix=basis_matrix_quadratic, roq_params=params,
        priors=priors, distance_marginalization=distance_marginalization, phase_marginalization=True)

    # write the weights to file so they can be loaded multiple times
    likelihood.save_weights(outdir+label+'_weights.json')

    # remove the basis matrices as these are big for longer bases
    del basis_matrix_linear, basis_matrix_quadratic

if __name__ == '__main__':
    if len(sys.argv)!=3:
        print("Usage is: \n > python roq_compute_weights.py NUMBER OUTDIR")
        print("NUMBER is the run number")
        print("OUTDIR is where the output will be written")
    else:
        main(int(sys.argv[1]), sys.argv[2])
