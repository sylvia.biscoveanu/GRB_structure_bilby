import numpy as np
from pandas import read_csv
import pdb

#Default values
thetaj_min = 2.
thetaj_max = 50.
dthetaj = 1.

k_min = 0.
k_max = 8.
dk = 0.1

gamma_min = 2.
gamma_max = 299.
dgamma = 3.

iota_min = 0.
iota_max = 90.
diota = 1.

thetajs = np.arange(thetaj_min, thetaj_max+dthetaj, dthetaj)
ks = np.arange(k_min, k_max+dk, dk)
gammas = np.arange(gamma_min, gamma_max+dgamma, dgamma)
iotas = np.arange(iota_min, iota_max+diota, diota)

# import and set up the table
table = read_csv('./param_table2_gamma.txt',header=None,delim_whitespace=True)
table = np.array(table).reshape(len(gammas),len(thetajs),len(ks),len(iotas))
table = np.nan_to_num(table)

#for fluence in ergs/cm^2
#iota much be in degrees
#Etot in ergs
#distance in cm

def cal_fluence(Etot, gamma, thetaj, k, iota, dist, z):
    ind_gamma = int(round((gamma-gamma_min)/dgamma))
    ind_thetaj = int(round((thetaj-thetaj_min)/dthetaj))
    ind_k = int(round((k-k_min)/dk))
    '''
    # Only needed when we start marginalizing over posteriors
    for i in iota:
        if (i > iotas[-1]):
            i = 180. - i
    '''
    ind_iota = int(round((iota-iota_min)/diota))
    ang = table[ind_gamma][ind_thetaj][ind_k][ind_iota]
    fluence = Etot*np.add(1,z)*np.array(ang)/(8*np.pi*np.power(dist,2))
    return fluence
