#!/usr/bin/env python                                                          


from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import bilby
from scipy.special import erf
import math
import sys
import pdb
import re

# write my own fast gaussian and truncated gaussian normalization functions
def gaus(vals,mu,sigma):
    exp = -(vals-mu)**2/(2*sigma**2)
    return 1./math.sqrt(2*np.pi*sigma**2)*np.exp(exp)

def norm(lower, upper, mu, sigma):
    return 0.5*(1+erf((upper-mu)/(math.sqrt(2)*sigma)))-0.5*(1+erf((lower-mu)/(math.sqrt(2)*sigma)))

def main(number, outdir, data_file, prior):

    # load the individual samples
    samples = []
    evidences = []
    for i in range(number):
        prior_name = re.split('_\d',re.split('/', data_file)[-1])[0]
        try:
            label = 'sgrb_' + str(i) + '_' + prior_name + '_'
            file_name = outdir+label+prior+'_result.json'
            results = bilby.core.result.read_in_result(file_name)
        except IOError:
            label = 'sgrb_'+prior_name+'_'+str(i)+'_'
            file_name = outdir+label+prior+'_result.json'
            results = bilby.core.result.read_in_result(file_name)
        results.posterior.pop('log_likelihood')
        samples.append(results.posterior)
        evidences.append(results.log_evidence)

    # define the initial run prior
    if prior=='topHatUniformElse':
        run_prior = bilby.core.prior.PriorSet(filename='./prior_files/topHatUniformElse.prior')

        # define the hyper prior relating the samples to the hyper parameters
        def hyper_prior(posteriors, mu_thetaj, sigma_thetaj, mu_log10_E0, sigma_log10_E0, mu_gamma, sigma_gamma):
            normfac = {'thetaj': norm(run_prior['thetaj'].minimum, run_prior['thetaj'].maximum, mu_thetaj, sigma_thetaj),
                    'log10_E0': norm(run_prior['log10_E0'].minimum, run_prior['log10_E0'].maximum, mu_log10_E0, sigma_log10_E0),
                    'gamma': norm(run_prior['gamma'].minimum, run_prior['gamma'].maximum, mu_gamma, sigma_gamma)}
            prob = gaus(posteriors['thetaj'], mu_thetaj, sigma_thetaj) / normfac['thetaj']*\
                    gaus(posteriors['log10_E0'], mu_log10_E0, sigma_log10_E0) / normfac['log10_E0']*\
                    gaus(posteriors['gamma'], mu_gamma, sigma_gamma) / normfac['gamma']
            return prob

        # uniform for all three distributions
        def sampling_prior(posteriors):
            return 1./((54-47)*(299-2)*(50-2))

        hp_prior = bilby.core.prior.PriorSet(filename='./prior_files/hyper_pe.prior')
        true_mus_sigmas = {'mu_thetaj': 25, 'sigma_thetaj': 5, 'mu_log10_E0': 50, 'sigma_log10_E0': 1, 'mu_gamma': 100, 'sigma_gamma': 50}

    elif prior=='uniformAll':
        log10_E0 = bilby.core.prior.Uniform(name='log10_E0', minimum=47, maximum=54, latex_label=r'$\logE_{0}$')
        gamma = bilby.core.prior.Uniform(name='gamma', minimum=2, maximum=299, latex_label=r'$\Gamma$')
        thetaj = bilby.core.prior.Uniform(name='thetaj', minimum=2, maximum=50, latex_label=r'$\theta_{j}$')

        def correlation_func_k(extrema_dict, gamma, thetaj):
            kMax = -np.log(1./gamma)/np.log(thetaj)
            return extrema_dict['minimum'], kMax

        k = bilby.core.prior.CorrelatedUniform(minimum=0, maximum=2, name='k', correlation_func=correlation_func_k)
        run_prior = bilby.core.prior.CorrelatedPriorDict(dictionary=dict(log10_E0=log10_E0, gamma=gamma, thetaj=thetaj, k=k))

        # define the hyper prior relating the samples to the hyper parameters
        def hyper_prior(posteriors, mu_thetaj, sigma_thetaj, mu_log10_E0, mu_k, sigma_log10_E0, mu_gamma, sigma_gamma, sigma_k):
            normfac = {'thetaj': norm(run_prior['thetaj'].minimum, run_prior['thetaj'].maximum, mu_thetaj, sigma_thetaj),
                    'log10_E0': norm(run_prior['log10_E0'].minimum, run_prior['log10_E0'].maximum, mu_log10_E0, sigma_log10_E0),
                    'gamma': norm(run_prior['gamma'].minimum, run_prior['gamma'].maximum, mu_gamma, sigma_gamma),
                    'k': norm(0, 8, mu_k, sigma_k)}
            prob = gaus(posteriors['thetaj'], mu_thetaj, sigma_thetaj) / normfac['thetaj']*\
                    gaus(posteriors['log10_E0'], mu_log10_E0, sigma_log10_E0) / normfac['log10_E0']*\
                    gaus(posteriors['gamma'], mu_gamma, sigma_gamma) / normfac['gamma']*\
                    gaus(posteriors['k'], mu_k, sigma_k) / normfac['k']
            return prob
        
        sampling_prior = None
        hp_prior = bilby.core.prior.PriorSet(filename='./prior_files/hyper_pe_uniformAll.prior')
        true_mus_sigmas = {'mu_thetaj': 7, 'sigma_thetaj': 4, 'mu_log10_E0': 50, 'sigma_log10_E0': 1, 'mu_gamma': 270, 'sigma_gamma':20, 'mu_k':1.9, 'sigma_k':0}

    hp_likelihood = bilby.hyper.likelihood.HyperparameterLikelihood(posteriors=samples, hyper_prior=hyper_prior,
                            sampling_prior=sampling_prior, log_evidences=evidences)

    result = bilby.core.sampler.run_sampler(likelihood=hp_likelihood, priors=hp_prior, sampler='dynesty', nlive=1000, walks=40, use_ratio=False,
                            outdir=outdir, label=prior_name+'_'+str(i)+'_hyper_dyn', verbose=True, resume=True)

    result.plot_corner(truth=true_mus_sigmas)
    
if __name__ == "__main__":
    if (len(sys.argv) != 5):
        print "Usage is: \n > python call_bilby_hyper.py EVENT_NUMBER OUTDIR DATA_FILE PRIOR"
        print "EVENT_NUMBER is the number of events to include in the analisys"
        print "OUTDIR is the directory where the output of the first step of PE is stored"
        print "DATA_FILE is the *sim.json file with the injection of the last event you want to include"
        print "PRIOR is either topHatUniformElse or uniformAll"
    else:
        main(int(sys.argv[1]), sys.argv[2], sys.argv[3], sys.argv[4])
